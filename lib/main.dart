import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:soscity/Pages/cadastro.dart';
import 'package:soscity/Pages/inicio.dart';
import 'package:soscity/Pages/recuperar_senha.dart';
import 'package:soscity/Global/internet.dart';
import 'Objects/User.dart';
import 'Objects/Location.dart';
import 'Global/CommonMethods.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SOS CITY',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Login(),
      initialRoute: '/login',
      routes: {
        '/login': (context) => Login(),
        '/inicio': (context) => Inicio(),
        //'/cadastro': (context) => Cadastros(),

      },

    );
  }
}

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  var _loading = false;
  bool _showSenha = false;

  var cpfController = TextEditingController();
  var senhaController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3b5998),
      appBar: AppBar(
        title: Text(
          'SOS CITY',
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  child: FloatingActionButton(
                      child: Icon(Icons.account_circle),
                      backgroundColor: Colors.green),
                  margin: EdgeInsets.only(bottom: 10, top: 10),
                ),
                Text(
                  'Login',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 20),
                        child: Text(
                            'Preencha os campos abaixo para realizar o login.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18)),
                      ),
                      TextField(
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          CpfInputFormatter(),
                        ],
                        autofocus: true,
                        keyboardType: TextInputType.number,
                        controller: cpfController,
                        decoration: InputDecoration(
                          labelText: "CPF: ",
                          prefixIcon: Padding(
                            child: Icon(Icons.perm_identity),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          controller: senhaController,
                          obscureText: !_showSenha,
                          onChanged: (String t) {
                            setState(() {});
                          },
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            labelStyle: null,
                            hintText: 'Digite sua senha aqui',
                            hintStyle: null,
                            prefixIcon: Padding(
                              child: Icon(Icons.vpn_key),
                              padding: EdgeInsets.all(5),
                            ),
                            suffixIcon: IconButton(
                                icon: Icon(Icons.remove_red_eye),
                                color: _showSenha ? Colors.red : Colors.blue,
                                onPressed: () => setState(() {
                                      _showSenha = !_showSenha;
                                    })),
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => RecuperarSenha())),
                              child: Text(
                                'Esqueci a senha',
                                style: TextStyle(color: Colors.blueAccent),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Center(
                          child: RaisedButton(
                            onPressed: _fazerLogin,
                            color: Colors.blue,
                            child: Text(
                              "Entrar",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => Cadastros())),
                              child: Text(
                                'Não possui conta? Faça seu cadastro',
                                style: TextStyle(color: Colors.blueAccent),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
           Visibility(visible: _loading, child: Container(color: Colors.white, child: Center(child: CircularProgressIndicator(),),),),
          ],
        ),
      ),
    );
  }

  _fazerLogin() async {
    // esconde o teclado
    FocusScope.of(context).unfocus();

    try {
      // campos obrigatorios
      if (cpfController.text.trim().isEmpty || senhaController.text.isEmpty)
        throw ('Todos os campos são obrigatórios.');

      //se a senha for menor que 8 digitos
      if (senhaController.text.trim().length < 8)
        throw ('A senha deve ter pelo menos 8 caracteres.');

      setState(() {
        _loading = true;
      });


      await Internet.post('user/login', // url
          {
            'cpf': cpfController.text.trim(),
            'senha': senhaController.text.trim(),
          }).then((r) {
        if (r['failed'] == null) {
          final location =
              new Location(int.parse(r['endereco']), r['coordenadas']);
          final usuario = new User(
              int.parse(r['user']),
              r['nome'],
              r['cpf'],
              DateTime.parse(r['nascimento']),
              r['email'],
              r['senha'],
              location);
          print("User:" + usuario.id.toString());
          //progress.showWithText('Loading...');

          setState(() {
            _loading = false;
          });

          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (_) => Inicio(user: usuario)));
        } else {
          CommonMethods.showDialogBox('Atenção', r['failed'], context);
          setState(() {
            _loading = false;
          });
        }
      });
    } catch (e) {

      // teste
      setState(() {
        _loading = false;
      });

      CommonMethods.showDialogBox('Atenção', e.toString(), context);
    }
  }
}
