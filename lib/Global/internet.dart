import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

final String baseUrl = 'http://soscity.com.br/';

class Internet {
  static String error = '';
  static String status = '';
  static String msg = '';
  static String stringData = '';

  // post
  static Future post(path, Map dataMap) async {
    try {
      String url = baseUrl + path;
      print('URL: ' + url);
      print('Data to send: ' + dataMap.toString());
      var r = await http.post(
        Uri.encodeFull(url),
        body: dataMap,
        //headers: {'Content-Type': "application/x-www-form-urlencoded"}
      );
      print('StatusCode: ' + r.statusCode.toString());
      print('Returned: ' + r.body);
      if (r.body.toString().isNotEmpty) {
        Map<String, dynamic> json = jsonDecode(r.body);
        Internet.status = json['s'];
        Internet.msg = json['msg'];
        Internet.stringData = json['d'].toString();
        print('Returned: ' + json.toString());
        return json;
      } else
        throw new Exception('No data returned.');
    } catch (e) {
      Internet.error = e.toString();
    }
    return null;
  }

  static Future customGET(var url) async {
    print('URL: ' + Uri.encodeFull(url));
    var r = await http.get(Uri.encodeFull(url));
    print('Returned: ' + r.body.toString());
    try {
      print(r.statusCode);
      if (r.statusCode != 200 && r.statusCode != 403)
        throw Exception('Status != 200');
      if (r.body.isEmpty) throw Exception('Nada retornado');
      var a = jsonDecode(r.body);
      if (a['erro'] != null) throw Exception('CEP não encontrado.');
      return a;
    } catch (e) {
      Internet.error = e.toString();
      print('ERRO: ' + e.toString());
      return null;
    }
  }

  // get
  static Future get(path) async {
    try {
      String url = baseUrl + path;
      print('URL: ' + url);
      var r = await http.get(Uri.encodeFull(url));
      print('StatusCode: ' + r.statusCode.toString());
      print('Returned: ' + r.body);
      //final parsed = json.decode(r.body).cast<Map<String, dynamic>>();
      //return parsed.map<Product>((json) => Product.fromMap(json)).toList();
      return json.decode(r.body);
    } catch (e) {
      print(e.toString());
    }
    return null;
  }
}
