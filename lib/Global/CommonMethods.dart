import 'package:flutter/material.dart';
import 'package:soscity/Objects/User.dart';

class CommonMethods {
  static User user;

  static String dateToStringEuaFormat(DateTime date) {
    var dayString = date.day.toString();
    var monthString = date.month.toString();

    if (int.parse(dayString) < 10) dayString = "0" + dayString.toString();
    if (int.parse(monthString) < 10) monthString = "0" + monthString.toString();

    return "" + date.year.toString() + "-" + monthString + "-" + dayString + "";
  }

  static String dateToStringBrFormat(DateTime date) {
    var dayString = date.day.toString();
    var monthString = date.month.toString();

    if (int.parse(dayString) < 10) dayString = "0" + dayString.toString();
    if (int.parse(monthString) < 10) monthString = "0" + monthString.toString();
    print("" + dayString + "/" + monthString + "/" + date.year.toString() + "");
    return "" + dayString + "/" + monthString + "/" + date.year.toString() + "";
  }

  static showDialogBox(String s, r, BuildContext context) {
    _showDialog(s, r, context);
    return true;
  }

  static _showDialog(String s, r, BuildContext bcontext) {
    showDialog(
      context: bcontext,
      builder: (BuildContext context) {
        // retorna um objeto do tipo Dialog
        return AlertDialog(
          title: new Text(s),
          content: new Text(r),
          actions: <Widget>[
            // define os botões na base do dialogo
            new FlatButton(
              child: new Text("OK"),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}
