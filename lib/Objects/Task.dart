import 'package:soscity/Global/internet.dart';
import 'dart:convert';
import 'package:soscity/Objects/Location.dart';
import 'package:soscity/Objects/User.dart';

class Task {
  int _id;
  String _descricao, _titulo;
  int _minParticipantes, _categoria = 0;
  DateTime _abertura;
  DateTime _encerramento;
  List<Location> _locations;
  Location _location;
  bool _participa = false;

  //User _owner;
  static List<Task> _chamadas;
  Task(
    this._id,
    this._titulo,
    this._descricao,
    this._minParticipantes,
    this._abertura,
    this._encerramento,
    this._location,
    //Location(int id, String coordenadas),
  ) {
    print(true);
  }

  static set chamadas(List<Task> content) {
    _chamadas = content;
  }

  static List<Task> get chamadas {
    return _chamadas;
  }

  get id {
    return _id;
  }

  bool get participa {
    return _participa;
  }

  /*set participa(bool par) {
    _participa = par;
  }*/

  void alteraParticipacao(bool par) {
    _participa = par;
  }

  get abertura {
    return _abertura;
  }

  get encerramento {
    return _encerramento;
  }

  get descricao {
    return _descricao;
  }

  get minParticipantes {
    return _minParticipantes;
  }

  get titulo {
    return _titulo;
  }

  get endereco {
    return _location.coordenadas;
  }

  static CreateChamadaFromJson(json) {}

  static getChamadas() async {
    //print("Algo vai acontecer");
    Internet.post('task', {'s': ''}).then((r) {
      //print("Algo aconteceu!" + r.toString());
      /*if (r['s'] != 'ok') {
        print("Algo deu ruim");
        print('Erro: ' + r['m']);
      } else {
        print("Algo deu bom");
        print("Saída do GET" + r.toString());
        return r;
      }*/
      return r;
    });
  }

  String toFullJson() {
    return '{"idTask":"' +
        _id.toString() +
        '","descricao":' +
        _descricao.toString() +
        ',"minimoDeParticipantes":"' +
        _minParticipantes.toString() +
        ',"abertura":"' +
        _abertura.toString() +
        ',"encerramento":"' +
        _encerramento.toString() +
        ',"idLocation":"' +
        _locations[0].id.toString() +
        '","enderecoCoordenadas":"' +
        _locations[0].coordenadas.toString() +
        '"}';
  }
}

class ListaChamadas {
  List<Task> chamadas;

  ListaChamadas({this.chamadas});

  factory ListaChamadas.fromJSON(json) {
    List<Task> lista = List();
    for (dynamic item in json) {
      Map<String, dynamic> converted = item as Map<String, dynamic>;
      //print("Converted:" + converted.toString());
      var idTemp = int.parse(converted['idTask']);
      //print("var: " + idTemp.toString());
      var tituloTemp = converted['titulo'];
      var descTemp = converted['descricao'];
      var minTemp = int.parse(converted['MinParticipantes']);
      var aberturaTemp = DateTime.parse(converted['abertura']);
      var encerramentoTemp = DateTime.parse(converted['encerramento']);
      var idLocationTemp = int.parse(converted['idLocation']);
      var coordenadasTemp = converted['coordenadas'];
      var novaChamada = new Task(
          idTemp,
          tituloTemp,
          descTemp,
          minTemp,
          aberturaTemp,
          encerramentoTemp,
          new Location(idLocationTemp, coordenadasTemp));
      //print("Formou Chamada: ");
      lista.add(novaChamada);
      //print("Adicionou na LISTA");
    }
    return ListaChamadas(chamadas: lista);
  }
}
