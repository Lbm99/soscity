class Location{
  int _id;
  String _coordenadas;

  Location(this._id, this._coordenadas);

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  get coordenadas{
    return _coordenadas;
  }

  set coordenadas(String value) {
    _coordenadas = value;
  }
}