import 'Location.dart';

class User {
  int _id;
  String _nome, _email, _senha, _cpf;
  DateTime _nascimento;
  Location _endereco;

  User(this._id, this._nome, this._cpf, this._nascimento, this._email,
      this._senha, this._endereco);

  // construtor de objetos UserModel a partir de objetos Json
  factory User.fromJson(Map<String, dynamic> t) {
    return User(
      int.parse(t['id']),
      t['nome'],
      t['cpf'],
      t['nascimento'],
      t['email'],
      t['senha'],
      t['endereco'],
    );
  }

  String toJson() {
    return '{"nome":"' +
        _nome +
        '","cpf":"' +
        _cpf +
        '","nascimento":"' +
        _nascimento.toString() +
        '","email":"' +
        _email +
        '","senha":"' +
        _senha +
        '","endereco":"' +
        _endereco.coordenadas +
        '"}';
  }

  Location get endereco => _endereco;

  set endereco(Location value) {
    _endereco = value;
  }

  DateTime get nascimento => _nascimento;

  set nascimento(DateTime value) {
    _nascimento = value;
  }

  get cpf => _cpf;

  set cpf(value) {
    _cpf = value;
  }

  get senha => _senha;

  set senha(value) {
    _senha = value;
  }

  get email => _email;

  set email(value) {
    _email = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  get id {
    print("ID:" + _id.toString());
    return _id;
  }

  set id(int value) {
    _id = value;
  }
}
