import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'inicio.dart';
import '../Global/CommonMethods.dart';
import '../Global/internet.dart';
import '../Objects/Task.dart';
import '../Pages/maps.dart';

class Chamada extends StatefulWidget {
  Chamada({@required this.chamada, @required this.user});

  final chamada, user;
  _Chamada createState() => _Chamada(chamada: this.chamada, user: user);
}

class _Chamada extends State<Chamada> {
  _Chamada({@required this.chamada, @required this.user});

  final chamada, user;

  String _textAction = "Participar";

  Future listenForChamada() async {
    await Internet.post("task/adepto", {
      's': '',
      //IdUser -- foi pra idUser... e task, mesma coisa
      'IdUser': user.id.toString(),
      'IdTask': chamada.id.toString(),
    }).then((r) {
      //print(json.decode(r.toString()));
      print("RESULTADO: " + r.toString());
      // trata o retorno que se encontra em r
      if (r['adepto']) {
        chamada.alteraParticipacao(true);
      }
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3b5998),
      appBar: AppBar(
        title: Text(
          chamada.titulo,
        ),
      ), //Título da Chamada
      body: Container(
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 250.0,
                        margin: EdgeInsets.only(bottom: 10, top: 10),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                'Título: ' + chamada.titulo,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    height: 1.5),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 400,
                                color: Colors.blueAccent,
                                child: Icon(Icons.photo),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Detalhes: ' + chamada.descricao + '\n',
                          style: TextStyle(fontSize: 20, color: Colors.black),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          child: Text(
                            'Mínimo de Participantes: ' +
                                chamada.minParticipantes.toString() +
                                '\nData de Abertura: ' +
                                CommonMethods.dateToStringBrFormat(
                                    chamada.abertura) +
                                '\nData de Encerramento: ' +
                                CommonMethods.dateToStringBrFormat(
                                    chamada.encerramento) +
                                '\n',
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                        ),
                      ),
                      Container(
                        child: Row(children: <Widget>[
                          Text(
                            'Ver Localização: ',
                            style: TextStyle(fontSize: 20, color: Colors.black),
                          ),
                          IconButton(
                            onPressed: () {
                              //CommonMethods.showDialogBox('OK', "Enviando ${chamada.endereco}", context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Maps(
                                            coordenadasIniciais:
                                                chamada.endereco,
                                            canEdit: false,
                                          )));
                            },
                            icon: Icon(Icons.location_on),
                            color: Colors.redAccent,
                            tooltip: "Ver no Maps",
                          ),
                        ]),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 60),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () => Navigator.pop(context),
                              color: Colors.redAccent,
                              child: Text(
                                'Voltar',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            //Esse future builder ainda não está funcionando.
                            //Ele serve para conferir a participação do usuário no banco de dados e
                            //substitui o raised button seguinte.
                            FutureBuilder(
                              future: listenForChamada(),
                              initialData: "Aguardando os dados...",
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting)
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                if (snapshot.hasData) {
                                  if (snapshot.data['adepto']) {
                                    _textAction = "Abandonar";
                                  }
                                  return RaisedButton(
                                    onPressed: _acao,
                                    color: chamada.participa
                                        ? Colors.red
                                        : Colors.green,
                                    child: Text(
                                      chamada.participa == true
                                          ? "Participar"
                                          : "Cancelar participação",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  );
                                } else {
                                  print(snapshot.error);
                                  return Text("");
                                }
                              },
                            ),
                            RaisedButton(
                              onPressed: _acao,
                              color: widget.chamada.participa == false
                                  ? Colors.green
                                  : Colors.red,
                              child: Text(
                                widget.chamada.participa == false
                                    ? "Participar"
                                    : "Abandonar",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  _acao() {
    try {
      Internet.post('task/manage', // url
          {
            's': '',
            'IdUser': user.id.toString(),
            'IdTask': chamada.id.toString(),
          }).then((r) {
        // trata o retorno que se encontra em r
        if (r['s'] == 'success') {
          CommonMethods.showDialogBox(
              'Alteração interrompida!', r['m'].toString(), context);
        } else {
          CommonMethods.showDialogBox(
              'OK', 'Alteração realizada com sucesso', context);
          chamada.alteraParticipacao;
        }
      });
    } catch (e) {
      CommonMethods.showDialogBox('Atenção', e.toString(), context);
    }
  }
}
