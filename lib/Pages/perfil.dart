import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:soscity/main.dart';
import 'inicio.dart';

class Perfil extends StatefulWidget {
  Perfil({
    @required this.user,
  });

  final user;

  @override
  _Perfil createState() => new _Perfil(user: user);
}

class _Perfil extends State<Perfil> {
  _Perfil({
    @required this.user,
  });
  final user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3b5998),
      appBar: AppBar(
        title: Text(
          'PERFIL',
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: FloatingActionButton(
                            child: Icon(Icons.account_circle), backgroundColor: Colors.green),
                      ),
                      Container(
                        //chamada dos dados via model
                        child: Text('Usuário: '+
                          user.nome,
                          style: TextStyle(
                              color: Colors.black, fontSize: 22, height: 1.5),
                        ),
                        margin: EdgeInsets.only(bottom: 10, top: 10),
                      ),
                      Container(
                        child: Text(
                            "Para editar seus dados, é necessário seguir os seguintes passos:\n"
                                "\n1. Acessar o site, clicando no botão abaixo:",
                            style: TextStyle(
                            color: Colors.black, fontSize: 20, height: 1.5),
                        ),
                      ),
                      Container(
                        child: new RaisedButton(
                          onPressed: () => openBrowserTab(),
                          child: new Text('Abrir site', style: TextStyle(
                              color: Colors.white, fontSize: 13, height: 1.5),),
                          color: Colors.deepPurple,
                        ),
                      ),
                      Container(
                        child: Text("2. Fazer o login com seus respectivos dados e, após isso, ir na aba PERFIL;\n"
                            "3. Ao acessar ela, haverão as opções de edição de seus dados, como endereço, senha.",
                            style: TextStyle(
                               color: Colors.black, fontSize: 20, height: 1.5),
                        ),
                      ),
//                      Container(
//                        margin: EdgeInsets.only(top: 20),
//                        child: Center(
//                          child: Text(
//                            "Colaborações feitas: ",
//                            textAlign: TextAlign.center,
//                            style: TextStyle(fontSize: 20),
//                          ),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(top: 20),
//                        child: Row(
//                            mainAxisAlignment: MainAxisAlignment.spaceAround,
//                            children: <Widget>[
//                              RaisedButton(
//                                onPressed: () => Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                        builder: (_) => Pendentes())),
//                                color: Colors.redAccent,
//                                child: Text(
//                                  'Pendentes',
//                                  style: TextStyle(color: Colors.white),
//                                ),
//                              ),
//                            ]),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(top: 10),
//                        child: Center(
//                          child: Text(
//                            "Colaborações feitas: ",
//                            textAlign: TextAlign.center,
//                            style: TextStyle(fontSize: 20),
//                          ),
//                        ),
//                      ),
                      Container(
                        margin: EdgeInsets.only(top: 60),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () => Navigator.pop(context),
                              color: Colors.redAccent,
                              child: Text(
                                'Voltar',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            RaisedButton(
                              onPressed: () => Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(builder: (_) => Login()),
                                  ModalRoute.withName('/inicio')),
                              color: Colors.redAccent,
                              child: Text(
                                'Logout',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }


  openBrowserTab() async {
    FlutterWebBrowser.openWebPage(
        url: "http://soscity.com.br/",
        customTabsOptions: CustomTabsOptions(
        colorScheme: CustomTabsColorScheme.dark,
        toolbarColor: Colors.deepPurple,
        secondaryToolbarColor: Colors.green,
        navigationBarColor: Colors.amber,
        addDefaultShareMenuItem: true,
        instantAppsEnabled: true,
        showTitle: true,
        urlBarHidingEnabled: true,
      )
    );
  }
  _logOut() {
    Navigator.popAndPushNamed(context, '/');
  }
}
