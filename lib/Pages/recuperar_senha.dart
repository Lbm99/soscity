import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:random_string/random_string.dart';
import '../Global/Internet.dart';
import '../Global/CommonMethods.dart';

class RecuperarSenha extends StatefulWidget {
  @override
  _RecuperarSenha createState() => _RecuperarSenha();
}

class _RecuperarSenha extends State<RecuperarSenha> {
  var _loading = false;
  var _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recuperar senha'),
      ),
      body: Container(
        color: Color(0xff3b5998),
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  child: FloatingActionButton(
                      child: Icon(Icons.lock), backgroundColor: Colors.green),
                  margin: EdgeInsets.only(top: 20, bottom: 20),
                ),
                Text(
                  'Recuperar senha',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 20),
                        child: Text(
                          'Insira o e-mail de cadastro e enviaremos uma nova senha.',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      TextField(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        onChanged: (v) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          hintText: 'Formato: conta@dominio.com',
                          prefixIcon: Padding(
                            child: Icon(Icons.email),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () => Navigator.pop(context),
                              color: Colors.red,
                              child: Text(
                                'Cancelar',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            RaisedButton(
                              onPressed: _novaSenha,
                              color: Colors.blue,
                              child: Text(
                                'Enviar nova senha',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Visibility(
              visible: _loading,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _novaSenha() async {
    var novaSenha = randomAlphaNumeric(10);
    try {
      // validação se o campo foi preenchido
      if (_emailController.text.trim().isEmpty ||
          !_emailController.text.contains('@') ||
          !_emailController.text.contains('.'))
        CommonMethods.showDialogBox(
            'Atenção', 'Informe o e-mail corretamente.', context);
      setState(() {
        _loading = true;
      });

      Internet.post('user/changep', {
        's': '',
        'email': _emailController.text.trim(),
        'senha': novaSenha,
      }).then((r) {
        // trata o retorno que se encontra em r
        if (r['s'] == 'success') {
          setState(() {
            _loading = false;
          });
          _showDialog('Concluído', 'Operação realizada com sucesso')
              .then((ret) {
            Navigator.popUntil(context, ModalRoute.withName('/login'));
          });
        } else {
          setState(() {
            _loading = false;
          });

          _showDialog('OK', 'E-mail enviado com sucesso! Confira sua caixa de entrada.').then((ret) {
            Navigator.popUntil(context, ModalRoute.withName('/login'));
          });
        }
      });
    } catch (e) {
      // teste
      setState(() {
        _loading = false;
      });
      CommonMethods.showDialogBox('Atenção', e.toString(), context);
    }
  }

  Future _showDialog(String t, String m) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // retorna um objeto do tipo Dialog
        return AlertDialog(
          title: new Text(t),
          content: new Text(m),
          actions: <Widget>[
            // define os botões na base do dialogo
            new FlatButton(
              child: new Text("OK"),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}

