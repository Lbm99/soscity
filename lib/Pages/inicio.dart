import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'perfil.dart';
import 'reclamacoes.dart';
import 'chamada.dart';
import '../Objects/Task.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../Global/CommonMethods.dart';

class Inicio extends StatefulWidget {
  Inicio({
    @required this.user,
  });

  final user;

  @override
  _Inicio createState() => new _Inicio(user: user);
}

class _Inicio extends State<Inicio> {
  ListaChamadas _chamadas;

  _Inicio({
    @required this.user,
  });

  final user;

  @override
  void initState() {
    super.initState();
  }

  Future<ListaChamadas> listenForChamadas() async {
    final data = await http.get("http://soscity.com.br/Task");
    return ListaChamadas.fromJSON(jsonDecode(data.body));
  }

  Widget build(BuildContext context) {
    Column _buildButtonColumn(IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      );
    }

//////////////////////////////////////////
//    filtros não serão utilizados
//    Widget filterSection = Container(
//      child: Row(
//        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//        children: [
//          Text("Ordenar por:"),
//          _buildButtonColumn(Icons.star, 'Relevância'),
//          _buildButtonColumn(Icons.access_time, 'Tempo'),
//          //_buildButtonColumn(Icons.star, 'ORDER'),
//        ],
//      ),
//    );

    Widget searchBar = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                icon: Icon(Icons.search),
                hintText: 'Buscar por chamada',
                border: InputBorder.none,
              ),
            ),
          )
        ],
      ),
    );

    List<Card> _generateItemCards() {
      List<Card> itemsDescription = List();

      if (_chamadas != null) {
        this._chamadas.chamadas.forEach((item) {
          itemsDescription.add(Card(
            child: Container(
              height: 350.0,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListTile(
                      title: Text(item.titulo),
                      subtitle: Text(
                          CommonMethods.dateToStringBrFormat(item.abertura) +
                              " - " +
                              CommonMethods.dateToStringBrFormat(
                                  item.encerramento)),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: 500,
                      color: Colors.blueAccent,
                      child: Icon(Icons.photo),
                      /*decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(""),
                      fit: BoxFit.cover,
                    )
                ),*/
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment
                          .center, // Align however you like (i.e .centerRight, centerLeft)
                      child: Text(item.descricao),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.remove_red_eye),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          Chamada(chamada: item, user: user)));
                            },
                          ),
                          SizedBox(width: 8.0),
                          Text(
                            "Visualizar",
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                ],
              ),
            ),
          ));
        });
      } else {
        print("Deu ruim!");
      }

      return itemsDescription;
    }

    return Scaffold(
      backgroundColor: Color(0xff3b5998),
      appBar: AppBar(
        title: Text(
          'SOS CITY',
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.account_box),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => Perfil(
                            user: user,
                          )
                  )
              );
            },
          ),
        ],
      ),
      body: Container(
        color: Color(0xff3b5998),
        child: new Column(
          children: <Widget>[
            new Container(
              height: 50.0,
              color: Colors.white,
              child: Row(children: <Widget>[
                Expanded(
                    child: new Column(children: <Widget>[
                        //DropdownExample(),
                        searchBar,
                      ],
                    ),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: FutureBuilder(
                future: listenForChamadas(),
                initialData: "Aguardando os dados...",
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting)
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  //print(snapshot);
                  if (snapshot.hasData) {
                    _chamadas = snapshot.data;
                    return ListView(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      children: _generateItemCards(),
                    );
                  } else {
                    print(snapshot.error);
                    return Text("Bad request");
                  }
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => Reclamacoes(
                      userOwner: user,
                    ))),
        child: Icon(Icons.add),
        backgroundColor: Colors.black,
      ),
    );
  }
}

//////////////////////////////////////////
// filtros não serão utilizados
//class DropdownExample extends StatefulWidget {
//  @override
//  _DropdownExampleState createState() {
//    return _DropdownExampleState();
//  }
//}
//////////////////////////////////////////


//////////////////////////////////////////
// filtros não serão utilizados
//class _DropdownExampleState extends State<DropdownExample> {
//  String _value;
//  Widget build(BuildContext context) {
//    return Container(
//      child: Row(
//        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//        children: [
//          Text("Filtrar por categoria:"),
//          DropdownButton<String>(
//            value: _value,
//            items: <String>['Todas', 'Lixo', 'Reforma', 'Meio Ambiente']
//                .map((String value) {
//              return new DropdownMenuItem<String>(
//                value: value,
//                child: new Text(value),
//              );
//            }).toList(),
//            onChanged: (String value) {
//              setState(() {
//                _value = value;
//              });
//            },
//            hint: Text('Selecionar categoria'),
//          )
//        ],
//      ),
//    );
//  }
//}
//////////////////////////////////////////
