import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
import 'package:soscity/Global/CommonMethods.dart';
import 'package:soscity/Pages/maps.dart';
import 'package:soscity/main.dart';
import 'package:soscity/Global/internet.dart';

import 'maps.dart';
import 'maps.dart';

class Cadastros extends StatefulWidget {
  @override
  _Cadastros createState() => _Cadastros();
}

class _Cadastros extends State<Cadastros> {
  var _loading = false;
  bool _showSenha = false;

  var nomeController = TextEditingController();
  var cpfController = TextEditingController();
  var datanascController = TextEditingController();
  var enderecoController = TextEditingController();
  var emailController = TextEditingController();
  var senhaController = TextEditingController();

  DateTime selectedDate = DateTime.now();

  @observable
  bool passwordVisible = false;

  @action
  void togglePassowrdVisibility() => passwordVisible = !passwordVisible;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cadastro'),
      ),
      body: Container(
        color: Color(0xff3b5998),
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Faça seu cadastro preenchendo corretamente os campos abaixo.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: TextField(
                          autofocus: true,
                          controller: nomeController,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            labelText: 'Nome',
                            hintText: 'Digite seu nome...',
                            prefixIcon: Padding(
                              child: Icon(Icons.account_circle),
                              padding: EdgeInsets.all(5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            CpfInputFormatter(),
                          ],
                          controller: cpfController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelText: 'CPF',
                            hintText: 'Digite somente números',
                            prefixIcon: Padding(
                              child: Icon(Icons.perm_identity),
                              padding: EdgeInsets.all(5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          onTap: () => _selectDate(context),
                          controller: datanascController,
                          decoration: InputDecoration(
                            labelText: 'Data de Nascimento',
                            prefixIcon: Padding(
                              child: Icon(Icons.calendar_today),
                              padding: EdgeInsets.all(5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          controller: emailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: 'E-mail',
                            hintText: 'Ex: email@email.com',
                            prefixIcon: Padding(
                              child: Icon(Icons.email),
                              padding: EdgeInsets.all(5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          onTap: () => _getCoordenadas(context),
                          controller: enderecoController,
                          decoration: InputDecoration(
                            labelText: 'Localização',
                            prefixIcon: Padding(
                              child: Icon(Icons.location_on),
                              padding: EdgeInsets.all(5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextField(
                          controller: senhaController,
                          obscureText: !_showSenha,
                          onChanged: (String t) {
                            setState(() {});
                          },
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            labelStyle: null,
                            hintText: '********',
                            hintStyle: null,
                            prefixIcon: Padding(
                              child: Icon(Icons.vpn_key),
                              padding: EdgeInsets.all(5),
                            ),
                            suffixIcon: IconButton(
                                icon: Icon(Icons.remove_red_eye),
                                color: _showSenha ? Colors.red : Colors.blue,
                                onPressed: () => setState(() {
                                  _showSenha = !_showSenha;
                                })),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RaisedButton(
                              color: Colors.blue,
                              child: Text(
                                'Cadastrar',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: _cadastrar,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () => Navigator.push(context,
                                  MaterialPageRoute(builder: (_) => Login())),
                              child: Text(
                                'Já possui conta? Faça o login',
                                style: TextStyle(color: Colors.blueAccent),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Visibility(
              visible: _loading,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _cadastrar() async {
    // esconde o teclado
    FocusScope.of(context).unfocus();
    try {
      // campos obrigatorios
      if (nomeController.text.trim().isEmpty ||
          emailController.text.trim().isEmpty ||
          datanascController.text.isEmpty ||
          cpfController.text.isEmpty ||
          enderecoController.text.isEmpty ||
          senhaController.text.isEmpty)
        throw ('Preencha corretamente os campos.');

      //validação de e-mails
      if (emailController.text.trim().isEmpty ||
          !emailController.text.contains('@') ||
          !emailController.text.contains('.'))
        throw ('Verifique se o e-mail foi digitado corretamente.');

      //nome menor que 5
      if (nomeController.text.trim().length < 5)
        throw ("Verifique se o nome completo foi digitado corretamente.");

      //se a senha for menor que 8 digitos
      if (senhaController.text.trim().length < 8)
        throw ('A senha deve ter pelo menos 8 caracteres.');

      setState(() {
        _loading = true;
      });

      // se chegou aqui, envia um post para o caminho com os dados
      await Internet.post('user/insert', // url
          {
            's': '',
            'idUser': '',
            'nome': nomeController.text.trim(),
            'cpf': cpfController.text.trim(),
            'nascimento': CommonMethods.dateToStringEuaFormat(selectedDate),
            'email': emailController.text.trim(),
            'endereco': enderecoController.text.trim(),
            'senha': senhaController.text.trim(),
            //campos necessários para o cadastro de usuários ser feito
          }).then((r) {
        // trata o retorno que se encontra em r
        if (r['s'] == 'success') {
          setState(() {
            _loading = false;
          });
          _showDialog('OK', 'Cadastro realizado com sucesso');
        } else {
          setState(() {
            _loading = false;
          });
          _showDialog('OK', 'Cadastro realizado com sucesso').then((ret) {
            Navigator.popUntil(context, ModalRoute.withName('/login'));
          });
        }
      });
    } catch (e) {
      setState(() {
        _loading = false;
      });
      _showDialog('Atenção', e.toString());
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1910),
      lastDate: DateTime(2099),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        datanascController.text =
            CommonMethods.dateToStringBrFormat(selectedDate);
      });
  }

  _getCoordenadas(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Maps(canEdit: true)),
    );
    enderecoController.text = result;
  }



  Future _showDialog(String t, String m) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // retorna um objeto do tipo Dialog
        return AlertDialog(
          title: new Text(t),
          content: new Text(m),
          actions: <Widget>[
            // define os botões na base do dialogo
            new FlatButton(
              child: new Text("OK"),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}
