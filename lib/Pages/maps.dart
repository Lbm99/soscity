//import 'dart:html';
//import 'dart:math';
//import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:soscity/Global/CommonMethods.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}

class Maps extends StatefulWidget {
  Maps({Key key, this.title, this.coordenadasIniciais, @required this.canEdit})
      : super(key: key);
  final String title;
  final String coordenadasIniciais;
  final bool canEdit;

  @override
  _Maps createState() => _Maps(
      coordenadasIniciais: this.coordenadasIniciais, canEdit: this.canEdit);
}

class _Maps extends State<Maps> {
  _Maps({this.coordenadasIniciais, @required this.canEdit});
  final coordenadasIniciais;
  final bool canEdit;

  @override
  void initState() {
    super.initState();
    inicialMarker(this.coordenadasIniciais);
  }

  GoogleMapController _controller;
  var _initialPosition;

  final List<Marker> markers = [];

  addMarker(cordinate) {
    if (this.canEdit)
      setState(() {
        markers.clear();
        markers.add(Marker(position: cordinate, markerId: MarkerId("0")));
      });
  }

  inicialMarker(coordenada) {
    if (coordenada != null) {
      var strings = coordenada.split("|");
      var latitude = double.parse(strings[0]);
      var longitude = double.parse(strings[1]);
      _initialPosition =
          CameraPosition(target: LatLng(latitude, longitude), zoom: 18);
      markers.add(Marker(
          position: LatLng(latitude, longitude), markerId: MarkerId("0")));
      //_controller.animateCamera(CameraUpdate.newLatLng(LatLng(strings[0], strings[1])));
    } else {
      _initialPosition =
          CameraPosition(target: LatLng(-22.948606, -46.558546), zoom: 18);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        initialCameraPosition: _initialPosition,
        mapType: MapType.normal,
        onMapCreated: (controller) {
          setState(() {
            _controller = controller;
          });
        },
        markers: markers.toSet(),
        onTap: (cordinate) {
          //clearMarkers();
          _controller.animateCamera(CameraUpdate.newLatLng(cordinate));
          addMarker(cordinate);
          print(cordinate.latitude.toString() +
              "|" +
              cordinate.latitude.toString());
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (markers[0] != null) {
            final latitude = markers[0].position.latitude;
            final longitude = markers[0].position.longitude;
            Navigator.pop(context, "$latitude|$longitude");
          } else {
            CommonMethods.showDialogBox(
                "Escolher outra",
                "Nenhuma localização selecionada",
                context); //Não está funcionando
          }
        },
        child: Icon(Icons.add_location),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
