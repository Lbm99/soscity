import 'package:soscity/main.dart';
import 'package:flutter/material.dart';
import 'package:soscity/Global/internet.dart';
import 'package:soscity/Global/CommonMethods.dart';
import 'inicio.dart';
import 'maps.dart';

class Reclamacoes extends StatefulWidget {
  final userOwner;
  Reclamacoes({
    @required this.userOwner,
  });

  @override
  _Reclamacoes createState() => _Reclamacoes(userOwner: userOwner);
}

class _Reclamacoes extends State<Reclamacoes> {
  var _loading = false;
  var _tituloController = TextEditingController();
  var _descricaoController = TextEditingController();
  var _localProblema = TextEditingController();
  var _qtdParticipantesController = TextEditingController();
  var _dataProblema = TextEditingController();
  var _dataEncerramento = TextEditingController();
  final userOwner;

  //controller que pegara a data conforme o dia
  //final dataController = TextEditingController(text: UtilData.obterDataDDMMAAAA(DateTime.now()));

  DateTime selectedDate = DateTime.now().add(new Duration(days: 7));

  _Reclamacoes({
    @required this.userOwner,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Incluir reclamação'),
      ),
      body: Container(
        color: Color(0xff3b5998),
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                ),
                Text(
                  'Preencha os campos abaixo para \n incluir uma reclamação no sistema:',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 23, color: Colors.white),
                ),
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Container(
//                    margin: EdgeInsets.only(bottom: 20),
                          ),
                      TextFormField(
                        controller: _tituloController,
                        autofocus: true,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: 'Digite o título:',
                          prefixIcon: Padding(
                            child: Icon(Icons.title),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),
                      TextFormField(
                        controller: _descricaoController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: 'Digite a descrição do problema:',
                          prefixIcon: Padding(
                            child: Icon(Icons.description),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),
//                  Container(
//                    child: new Column(
//                      children: <Widget>[
//                        new Container(
//                          margin: EdgeInsets.only(top: 20),
//                          color: Colors.white,
//                          child: Row(children: <Widget>[
//                            Expanded(
//                                child: new Column(children: <Widget>[
//                                  DropdownExample(),
//                            ])),
//                          ]),
//                        ),
//                      ],
//                    ),
//                  ),
                      TextField(
                        controller: _qtdParticipantesController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Quantidade mínima de participantes: ',
                          hintText: 'Ex: 20',
                          prefixIcon: Padding(
                            child: Icon(Icons.supervised_user_circle),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),

///////////////////////////////////////////////////////////////////////////
//                  botão de pegar a data - caso seja necessário
//                  Container(
//                    margin: EdgeInsets.only(top: 20),
//                    child: RaisedButton(
//                      onPressed: () => _selectDate(context), // Refer step 3
//                      child: Text(
//                        'Data de realização',
//                        style: TextStyle(
//                            color: Colors.black, fontWeight: FontWeight.bold),
//                      ),
//                      color: Colors.greenAccent,
//                    ),
//                  ),
/////////////////////////////////////////////////////////////////////////////

                      TextField(
                        controller: _localProblema,
                        onTap: () => _getCoordenadas(context),
                        decoration: InputDecoration(
                          labelText: 'Local do problema:',
                          prefixIcon: Padding(
                            child: Icon(Icons.location_on),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),
                      TextField(
                        controller: _dataEncerramento,
                        onTap: () => _selectDate(context),
                        decoration: InputDecoration(
                          labelText: 'Data de encerramento:',
                          prefixIcon: Padding(
                            child: Icon(Icons.calendar_today),
                            padding: EdgeInsets.all(5),
                          ),
                        ),
                      ),

                      ////////////////////////////////////////
                      //botão de anexar foto
                      /*Container(
                    margin: EdgeInsets.only(top: 20),
                    child: RaisedButton(
                      onPressed: () => {},
                      color: Colors.blue,
                      child: Text(
                        'Anexar foto',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),*/
                      ////////////////////////////////////////

                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () => Navigator.pop(context),
                              color: Colors.red,
                              child: Text(
                                'Cancelar',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            RaisedButton(
                              onPressed: _cadastrar,
                              color: Colors.green,
                              child: Text(
                                'Cadastrar',
                                style: TextStyle(color: Colors.white),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Visibility(
              visible: _loading,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getCoordenadas(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Maps(canEdit: true)),
    );
    _localProblema.text = result;
  }

  _cadastrar() async {
    try {
      // campos obrigatorios
      if (_descricaoController.text.trim().isEmpty ||
          _localProblema.text.isEmpty ||
          _qtdParticipantesController.text.isEmpty) // ||
        //_dataProblema.text.isEmpty)
        throw ('Preencha corretamente os campos.');

      //if (_qtdParticipantesController.length <= 15) throw ("É necessário 15 participantes para a tarefa");

      setState(() {
        _loading = true;
      });

      // se chegou aqui, envia um post para o caminho com os dados
      await Internet.post('task/insert', // url
          {
            's': '',
            'IdUser': userOwner.id.toString(),
            'titulo': _tituloController.text.trim(),
            'descricao': _descricaoController.text.trim(),
            'MinParticipantes': _qtdParticipantesController.text,
            'encerramento': CommonMethods.dateToStringEuaFormat(selectedDate),
            'endereco': _localProblema.text.trim(),
            'categoria': 0.toString(),
            //campos necessários para o cadastro de usuários ser feito
          }).then((r) {
        // trata o retorno que se encontra em r
        if (r['s'] == 'success') {
          setState(() {
            _loading = false;
          });
          CommonMethods.showDialogBox(
              'Reclamação inserida com sucesso', r['m'].toString(), context);
        } else {
          setState(() {
            _loading = false;
          });
          _showDialog('OK', 'Reclamação inserida com sucesso').then((ret) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (_) => Inicio(user: userOwner)));
          });
        }
      });
    } catch (e) {
      setState(() {
        _loading = false;
      });
      CommonMethods.showDialogBox('Atenção', e.toString(), context);
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: selectedDate,
      lastDate: selectedDate.add(new Duration(days: 365)),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _dataEncerramento.text =
            CommonMethods.dateToStringBrFormat(selectedDate);
      });
  }

  Future _showDialog(String t, String m) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // retorna um objeto do tipo Dialog
        return AlertDialog(
          title: new Text(t),
          content: new Text(m),
          actions: <Widget>[
            // define os botões na base do dialogo
            new FlatButton(
              child: new Text("OK"),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}

class _DropdownExampleState extends State<DropdownExample> {
  String _value;

  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          DropdownButton<String>(
            value: _value,
            items: <String>['Lixo', 'Reforma', 'Meio Ambiente', 'Outros']
                .map((String value) {
              return new DropdownMenuItem<String>(
                value: value,
                child: new Text(value),
              );
            }).toList(),
            onChanged: (String value) {
              setState(() {
                _value = value;
              });
            },
            hint: Text('Selecionar categoria'),
          )
        ],
      ),
    );
  }
}

class DropdownExample extends StatefulWidget {
  @override
  _DropdownExampleState createState() {
    return _DropdownExampleState();
  }
}
